
import UIKit
import CoreLocation
import UserNotifications

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    let locationManager = CLLocationManager()
    
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions:[UIApplicationLaunchOptionsKey : Any]? = nil) -> Bool {
        locationManager.delegate = self
        locationManager.requestAlwaysAuthorization()
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound]) {(accepted, error) in
            if !accepted {
                print("Notification access denied.")
            }
        }
        let action = UNNotificationAction(identifier: "parameters", title: "Go in the settings", options: [])
        let category = UNNotificationCategory(identifier: "myCategory", actions: [action], intentIdentifiers: [], options: [])
        UNUserNotificationCenter.current().setNotificationCategories([category])
        
        
        
        return true
    }
    
    func handleEvent1(forRegion region: CLRegion!) {
        
        let content = UNMutableNotificationContent()
        content.title = "You enter in WIFI areas"
        content.body = "You enter in WIFI area you can stop use your Data Cellular ! "
        content.sound = UNNotificationSound.default()
        content.categoryIdentifier = "myCategory"
        
    }
    
    func handleEvent2(forRegion region: CLRegion!) {
        
        let content = UNMutableNotificationContent()
        content.title = "You get out of Wifi area"
        content.body = "If you want continue to use internet you need to activate your Data Cellular ! "
        content.sound = UNNotificationSound.default()
        content.categoryIdentifier = "myCategory"
        
    }
    
}

func note(fromRegionIdentifier identifier: String) -> String? {
    let savedItems = UserDefaults.standard.array(forKey: PreferencesKeys.savedItems) as? [NSData]
    let geotifications = savedItems?.map { NSKeyedUnarchiver.unarchiveObject(with: $0 as Data) as? Geotification }
    let index = geotifications?.index { $0?.identifier == identifier }
    return index != nil ? geotifications?[index!]?.name : nil
}



extension AppDelegate: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didEnterRegion region: CLRegion) {
        if region is CLCircularRegion {
            handleEvent1(forRegion: region)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didExitRegion region: CLRegion) {
        if region is CLCircularRegion {
            handleEvent2(forRegion: region)
        }
    }
    
    
    
}

extension AppDelegate: UNUserNotificationCenterDelegate {
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        
        if response.actionIdentifier == "parameters" {
            let settingsURL = NSURL(string: UIApplicationOpenSettingsURLString)
            UIApplication.shared.open(settingsURL! as URL, options: [:], completionHandler: nil)
            
        }
    }
}

