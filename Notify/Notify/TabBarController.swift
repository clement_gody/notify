

import UIKit

class TabBarController: UITabBarController {
    
    //dernier problème sprint3

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func getAddViewController () -> AddViewController?
    {
        guard let viewControllers = viewControllers else {
            return nil
        }
        
        for controller in viewControllers
        {
            if let addNavigationController = controller as? AddNavigationController
            {
                if let addController = addNavigationController.viewControllers.first as? AddViewController
                {
                    return addController
                }
            }
        }
        
        return nil
    }
    


}
